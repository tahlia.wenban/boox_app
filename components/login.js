import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ScrollView,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Picker,
  Alert,
  Platform,
  StatusBar,
  Button,
} from 'react-native';
import axios from 'axios';
import { URL } from '../config';

export default function Login(props) {

  //======== state ========

  const [form, setValues] = useState({
    email: '',
    password: '',
  });

  const [ message , setMessage ] = useState('')

  //======== handleClick ========

  const handleClick = async() => {
    try {
      const response = await axios.post(`${URL}/users/login`, {
        email: form.email,
        password: form.password,
      });
      props.handlePageChange('home')
      if (response.data.ok) {
        AsyncStorage.setItem('token', JSON.stringify(response.data.token));
        // setTimeout(props.handlePageChange('home'), 10);
      }
      setMessage(`* ${response.data.message}`)
    } catch (error) {
      console.log(error);
    }
  };

  //onChange = onChangeText

  return (
  <View style={styles.container}>
    <View style={styles.top}>
      <Image
        style={styles.image}
        source={require('../assets/images/booxIllustration.png')}
      />
    </View>
    <View style={styles.main}>
    <TouchableOpacity style={styles.googlebutton}>
        <Text style={styles.text}> 
          sign in with google
        </Text>
      </TouchableOpacity>
      <Text styles={styles.subtext}>or</Text>
      <TextInput
        placeholder='email'
        style={styles.input}
        onChangeText={text => setValues({...form,email:text})}
        name="email"
        value={form.email}
      />
      <TextInput
        secureTextEntry={true}
        placeholder='password'
        style={styles.input}
        onChangeText={text => setValues({...form,password:text})}
        name="password"
        value={form.password}
      />
      <View>
        <Text style={styles.subtext}>{message}</Text>
      </View>
      <TouchableOpacity style={styles.button} 
                        onPress={handleClick}>
        <Text style={styles.text}> 
          log in 
        </Text>
      </TouchableOpacity>
      <View style={styles.bottom}>
        <TouchableOpacity>
          <Text style={styles.subtextLink}> 
            register
          </Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.subtextLink}> 
            forgot password
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5efe0',
  },
  top: {
    flex: 1,
    height: '30%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  main: {
    height: '70%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: '80%',
    height: '6%',
    backgroundColor: '#EBE5D8',
    marginTop: '5%',
    paddingLeft: 10
  },
  title: {
    fontSize: 50,
    marginBottom: 30,
    color: '#FCD953',
  },
  button: {
    backgroundColor: '#181F27',
    width: '25%',
    height: '7%',
    padding: 15,
    borderRadius: 90,
    marginTop: 15,
    marginBottom: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  googlebutton: {
    backgroundColor: '#181F27',
    width: '80%',
    height: '7%',
    padding: 15,
    borderRadius: 90,
    marginTop: 20,
    marginBottom: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    color: '#ffff',
    fontSize: 17
  },
  image: {
    width: 300,
    height: 70,
    resizeMode: 'contain',
    marginTop: 100
  },
  subtext: {
    marginTop: 10
  },
  subtextLink: {
    color: '#fcd953',
    fontSize: 20,
    marginBottom: 3
  },
  bottom: {
    alignItems: 'center',
    marginTop: 70
  }
});
