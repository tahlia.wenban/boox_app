import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ScrollView,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Picker,
  Alert,
  Platform,
  StatusBar,
  Button,
} from 'react-native';

export default function Nav(props) {
  return (
    <View style={styles.nav}>
      <TouchableOpacity onPress={() => props.handlePageChange('home')}>
        <Image
          style={styles.icons}
          source={require('../assets/images/nav/home2.png')}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => props.handlePageChange('spin')}>
        <Image
          style={styles.icons}
          source={require('../assets/images/nav/spin2.png')}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => props.handlePageChange('bookshelf')}>
        <Image
          style={styles.icons}
          source={require('../assets/images/nav/shelf2.png')}
        />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  nav: {
    height: '10%',
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  icons: {
    height: 25,
    width: 25,
  },
});
