import React, { useState, useEffect } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ScrollView,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Picker,
  Alert,
  Platform,
  StatusBar,
  Button,
} from 'react-native';

import Nav from './nav';

export default function Home(props) {
  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <Text style={styles.title}>bookshelf</Text>
      </View>
      <Nav handlePageChange={props.handlePageChange} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f5efe0',
  },
  main: {
    height: '90%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 50,
    marginBottom: 30,
    color: '#FCD953',
  },
});
