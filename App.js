import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { URL } from './config'

import {
  StyleSheet,
  View,
  Text,
  TextInput,
  ScrollView,
  AsyncStorage,
  ImageBackground,
  Image,
  TouchableOpacity,
  Picker,
  Alert,
  Platform,
  StatusBar,
} from 'react-native';
import Home from './components/home';
import Spin from './components/spin';
import Bookshelf from './components/bookshelf';
import Login from './components/login';

export default function App() {
  //================= FUNCTIONS =======================//

  const [page, setPage] = useState('');
  const [user, setUser] = useState(false);
  const [userInfo, setUserInfo] = useState({})
  
  //check when load the app if token in ASYNCstorage and verify it
  //change user value when logged

  const token = JSON.parse(AsyncStorage.getItem('token'))
  useEffect( () => {
    token === null
    ? setUser(false)
    : userContent()
  }, [])

  // ==================== CHANGE USER CONTENT ===================================

  const userContent = async () => {
    try {
      const response = await axios.post(`${URL}/users/verify_token`, {token})
      return response.data.ok 
      ? (
        setUser(true),
        setUserInfo({...userInfo,
                    username:response.data.username, 
                    email:response.data.email,
                    books:response.data.books
                   })
        ): setUser(false)
    } 
    catch( error ) {
      console.log( error )
    }
  }

  const logout = () => {
    AsyncStorage.removeItem('token');
    setUser(false)
  }

  // ==================== CHANGE BETWEEN PAGES ===================================

  const handlePageChange = e => {
    setPage(e);
  };

  // ==================== RENDER IF USER ===================================
  if(user){

    //================= HOME =======================//
    if (page === 'home') {
      return <Home handlePageChange={handlePageChange} />;
    }

    //================= SPIN =======================//
    if (page === 'spin') {
      return <Spin handlePageChange={handlePageChange} />;
    }

    //================= BOOKSHELF =======================//
    if (page === 'bookshelf') {
      return <Bookshelf handlePageChange={handlePageChange} />;
    }

  }else{

    //================= LOGIN =======================//
      if (page === 'login') {
        return <Login handlePageChange={handlePageChange} />;
      }

    //================= REGISTER =======================//

  }
}
